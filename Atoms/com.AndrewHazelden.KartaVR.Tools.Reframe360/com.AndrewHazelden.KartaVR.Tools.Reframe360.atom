Atom {
	Name = "KartaVR Tools | Reframe360 Ultra",
	Category = "KartaVR/Tools",
	Author = "Andrew Hazelden",
	Version = 4.5,
	Date = {2021, 3, 1},
	Description = [[<p>Reframe360 Ultra is a DCTL fuse that allows you to reframe your equirectangular/spherical/LatLong panoramic videos to create unique camera angles. This reframing effect is also called "Overcapture".</p>

<p>The code was ported by David Kohen (<a href="https://www.youtube.com/LearnNowFX">Learn Now FX</a>), and Andrew Hazelden (<a href="http://www.andrewhazelden.com/projects/kartavr/docs/">KartaVR for Fusion</a>) from the original open-source <a href="https://github.com/stefsietz/reframe360resolve">Reframe360 Resolve</a> OpenFX plugin by Stefan Sietzen.</p>

<h2>DCTL Fuse Support Requirements</h2>

<ul>
	<li>An OpenCL, CUDA, or Metal based GPU</li>
	<li>Fusion Studio 16-17.1+ or Resolve 16-17.1+</li>
</ul>

<h2>Example Fusion Comp</h2>
<p>When this atom package is installed you can explore the example Fusion composites in this folder:</p>

<pre><a href="file:///Reactor:/Deploy/Comps/KartaVR/Reframe360/">Reactor:/Deploy/Comps/KartaVR/Reframe360/</a></pre>

<h2>Open Source Software License Term</h2>
<p>The Reframe360 Ultra fuse is Apache 2.0 licensed.</p>

<h2>Email Andrew Hazelden</h2>
<p><a href="mailto:andrew@andrewhazelden.com">andrew@andrewhazelden.com</a></p>
]],
	Deploy = {
		"Comps/KartaVR/Reframe360/Reframe360 Projection Comparison.comp",
		"Comps/KartaVR/Reframe360/Reframe360 Tiny Planet Barrens.comp",
		"Comps/KartaVR/Reframe360/Reframe360 Tiny Planet Forest.comp",
		"Fuses/KartaVR/kvrReframe360Ultra.fuse",
		"Macros/KartaVR/Images/latlong_barrens_sunset.jpg",
		"Macros/KartaVR/Images/latlong_nightsky_in_forest.jpg",
		"Templates/Edit/Effects/KartaVR/Viewer/kvrReframe360Ultra.setting",
		"Templates/Fusion/KartaVR/Viewer/kvrReframe360Ultra.setting",
	},
}
