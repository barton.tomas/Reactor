Atom {
	Name = "Make Local Button Script",
	Category = "Scripts",
	Author = "Bryan Ray",
	Version = 2.44,
	Date = {2020, 09, 17},
	Description = [[<p>The "Make Local Button Script" package adds a custom Button Control to your Loader nodes via a Default setting file. You can use the "Make Local" button to copy the media in the Loader into a Comp PathMap managed folder location.</p>
	<p>The script, which will be found in the /Reactor/Deploy/Scripts/support folder, is easily modified for use in your pipeline. Instructions are in the comments at the beginning of the Loader_MakeLocal.lua file.</p>
	<p>Without customization, the script will copy assets to a sub-folder called 'elements' in the same folder as the .comp file. The composition must be saved before the button will work.</p>
	<p>Because the script is designed to run only from the button, it does not show up in any of the typical script menus within Fusion.</p>
	<p>As a part of the installation process, this Atom will attempt to detect existing Loader default setting files, and if any are found, it will insert the button into those instead of creating a new default. When removed, the button is deleted from all setting files, but any installed Defaults will not be removed, in case you have made additional changes later.]],
	Donation = {
		URL = [[paypal.me/BryanRayVFX]],
		Amount = "$2 USD",
	},
	Deploy = {
		"Scripts/support/Loader_MakeLocal.lua",
		"Defaults/Loader_Loader.temp",
	},
	InstallScript = {[==[
		dprintf("\n[Make Local] Installing Defaults File\n\n")

		-- UserControl to be inserted into Loader_Loader.setting
		local insertString = [=[UserControls = ordered() {
				MakeLocal = {
					LINKS_Name = "Make Local",
					INPID_InputControl = "ButtonControl",
					BTNCS_Execute = [[
						args = { tool = comp.ActiveTool, copyTree = true }
						path = comp:MapPath("Scripts:support/Loader_MakeLocal.lua")
						comp:RunScript(path, args)
					]],
					IC_ControlPage = 0,
					LINKID_DataType = "Number",
					INP_Default = 0,
				},
]=]

		local defaultFound = false
		local platform = (FuPLATFORM_WINDOWS and "Windows") or (FuPLATFORM_MAC and "Mac") or (FuPLATFORM_LINUX and "Linux")
		local osSeparator = package.config:sub(1,1)
		-- Search Defaults pathmaps
		dprintf("[Defaults Folders] Scanning PathMaps\n")
		for i, path in ipairs(fu:MapPathSegments("defaults:")) do
			dprintf("\t[" .. i .. "] [Folder] " .. path .. "\n")
			for j, file in ipairs(bmd.readdir(path .. "*.setting")) do
				dprintf("\t[" .. i .. "] [Setting File] [" .. j .. "] " .. file.Name .. "\n")
				-- If a Loader default is found, insert the ButtonControl
				if file.Name == "Loader_Loader.setting" then
					defaultFound = true

					dprintf("[Setting file found] "..file.Name .. "\n")

					-- read setting file to memory
					local f = io.open(path .. file.Name, "r")

					if f then
						data = f:read("*all")

						-- Scan the file for a UserControls section
						k, l = string.find(data, "UserControls = ordered%(%) {")
							if l then
								-- Insert button into existing UserControls table
								dprintf("[Loader_Loader.setting] UserControls detected.\n")
								data = string.gsub(data, "UserControls = ordered%(%) {", insertString)
							else
								-- Insert new UserControls table
								dprintf("[Loader_Loader.setting] No UserControls.\n")
								data = string.gsub(data, "CtrlWZoom = false,", "CtrlWZoom = false,\n			" .. insertString .. "          },")
							end

						-- Close the file, then reopen to overwrite
						f:close()
						f = io.open(path..file.Name, "w")
						if f then
							io.output(f)
							io.write(data)
							io.close(f)
						else
							dprint("[Loader_Loader.setting] Unable to open the file for overwriting.\nCreating new Loader_Loader.setting file in Reactor folder.")
							defaultFound = false
						end
					else
						dprintf("[Loader_Loader.setting] Unable to open the file for editing.\nCreating new Loader_Loader.setting file in Reactor folder.")
						defaultFound = false
					end
				end
			end
		end

		local path = fu:MapPath("Reactor:")..'Deploy'..osSeparator..'Defaults'..osSeparator
		local command = ''

		-- If a setting file was modified, delete the temp file in the Reactor folder. Otherwise, rename it.
		if defaultFound == false then
			dprintf("[Loader_Loader.setting] No existing default files found. Renaming temp file.")
			-- Rename Loader_Loader.temp to Loader_Loader.setting
			if platform == "Windows" then
				command = 'ren \"'..path..'Loader_Loader.temp\" Loader_Loader.setting'
			elseif platform == "Mac" or "Linux" then
				command = "mv \'"..path.."Loader_Loader.temp\' \'"..path.."Loader_Loader.setting\'"
			end
		else
			dprintf("[Loader_Loader.temp] Button insertion successful. Deleting temp file.")
			-- Delete Loader_Loader.temp
			if platform == "Windows" then
				command = 'del \"'..path..'Loader_Loader.temp\"'
			elseif platform == "Mac" or "Linux" then
				command = "rm \'"..path.."Loader_Loader.temp\'"
			end
		end
		dprintf("\n\nPerforming os.execute("..command..")")
		os.execute(command)
	]==],
	},
	UninstallScript = {[==[dprintf("\n[Make Local] Uninstalling Defaults File\n\n")
		local osSeparator = package.config:sub(1,1)

		local defaultFound = false
		-- The UserControl to be removed
		local DeleteString = [=[				MakeLocal = {
					LINKS_Name = "Make Local",
					INPID_InputControl = "ButtonControl",
					BTNCS_Execute = [[
						args = { tool = comp.ActiveTool, copyTree = true }
						path = comp:MapPath("Scripts:support/Loader_MakeLocal.lua")
						comp:RunScript(path, args)
					]],
					IC_ControlPage = 0,
					LINKID_DataType = "Number",
					INP_Default = 0,
				},			]=]
		-- Find all existing Loader default setting files
		dprintf("[Defaults Folders] Scanning PathMaps\n")
		for i, path in ipairs(fu:MapPathSegments("defaults:")) do
			dprintf("\t[" .. i .. "] [Folder] " .. path .. "\n")
			for j, file in ipairs(bmd.readdir(path .. "*.setting")) do
				dprintf("\t[" .. i .. "] [Setting File] [" .. j .. "] " .. file.Name .. "\n")
				if file.Name == "Loader_Loader.setting" then
					defaultFound = true

					dprintf("[Setting file found] "..file.Name .. "\n")

					-- read file to memory
					local f = io.open(path .. file.Name, "r")

					if f then
						data = f:read("*all")

						-- Delete the MakeLoader button
						startIndex, _ = string.find(data, "MakeLocal = ")
						local newdata = string.sub(data, 1, startIndex-1)..string.sub(data, startIndex+374, string.len(data))

						-- Close the file, then reopen to overwrite
						f:close()
						f = io.open(path..file.Name, "w")
						if f then
							io.output(f)
							io.write(newdata)
							io.close(f)
						else
							dprint("[Loader_Loader.setting] Unable to open the file for overwriting.\nCreating new Loader_Loader.setting file in Reactor folder.")
							defaultFound = false
						end
					else
						dprintf("[Loader_Loader.setting] Unable to open the file for editing.\nCreating new Loader_Loader.setting file in Reactor folder.")
						defaultFound = false
					end
				end
			end
		end
	]==],
	},
}