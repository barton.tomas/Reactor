comp:Lock()

saver_plus = comp:AddTool('Saver', -32768, -32768)

local ctrls = saver_plus.UserControls

ctrls.SOLO = {
    LINKID_DataType = "Number",
    INP_Default = 0,
    INPID_InputControl = "ButtonControl",
    BTNCS_Execute = [[    
        function check_selected(tool)
            return tool:GetAttrs('TOOLB_Selected')
        end

        function check_enabled(tool)
            return tool:GetAttrs('TOOLB_PassThrough')
        end

        local comp = fu:GetCurrentComp()
        local selectedSavers = comp:GetToolList(true, "Saver")
        local allSavers = comp:GetToolList(false, "Saver")

        comp:StartUndo("Solo Saver")
        
        for _, currentSaver in pairs(allSavers) do
            if not check_selected(currentSaver) then
                currentSaver:SetAttrs( { TOOLB_PassThrough = true } )
            end
        end
        
        for _, sel in pairs(selectedSavers) do
            if check_enabled(sel) then
                sel:SetAttrs({ TOOLB_PassThrough = false})
            end
        end 
        comp:EndUndo()
    ]],
    LINKS_Name = "Solo",
    ICS_ControlPage = "File",
    }
    ctrls.ML = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Comp/Saver Tools/LoaderFromSaver.lua", tool) ]],
        LINKS_Name = "Make Loader",
        ICS_ControlPage = "File",
    }

    ctrls.VersionUP = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionUp.py", tool) ]],
        LINKS_Name = "Version UP",
        ICS_ControlPage = "File",
    }
    ctrls.VersionDOWN = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionDown.py", tool) ]],
        LINKS_Name = "Version DOWN",
        ICS_ControlPage = "File",
    }
saver_plus.UserControls = ctrls
refresh = saver_plus:Refresh()

comp:Unlock()
