----------------------------------------------------------------------------
-- Class Browser 1.2.3 for DaVinci Resolve/Fusion v16+ (and Fusion v9)
--  roger.magnusson@gmail.com
--
--  v1.2.3 2021-03-18
--   * Fixed: Startup crash in Resolve/Fusion v17.1 caused by class types that are no longer available
--   * Fixed: Classes containing only ignored discovered methods (like _cast) caused properties to be skipped
--   * Fixed: FuPath methods weren't being discovered
--
--  v1.2.2 2020-05-14
--   * Fixed: The Plugins path was incorrect for Fusion on Windows
--
--  v1.2.1 2020-05-13
--   * New: Additional properties discovered automatically
--   * New: Several more files are probed for methods and properties
--   * Fixed: Some PreviewControl classes weren't showing in the nested view
--
--  v1.2 2020-05-10
--   * New: Tag Maps and Enum Maps
--   * New: Additional methods that aren't documented in the help system are now discovered automatically
--   * Fixed: Fusion v9 window layout issue
--   * Fixed: Fusion v9 didn't show property types when available
--   * Fixed: Fusion v9 error when registry entries contained tables, added workaround for dumptostring() bug
--
--  v1.1 2020-04-02
--   * Added support for showing registry entries associated with a class
--
--  v1.0 2020-02-23
--
--   Inspired by Andrew Hazeldens
--    "FusionScript Help Browser" available in the
--    WSL Reactor package manager:
--    https://www.steakunderwater.com/wesuckless/viewtopic.php?f=32&t=3067
--
----------------------------------------------------------------------------

local ui = app.UIManager
local dispatcher = bmd.UIDispatcher(ui)
local classes = {}
local tag_map = {}
local enum_map = {}
local discovered_class_methods = {}
local discovered_class_properties = {}
local registry_index = {}
local class_type_index = {}
local window = {}
local windowItems = {}
local splash_window = {}
local splash_items = {}
local is_filtered = false
local script_name = "Class Browser 1.2.3"

-- Gets integer values for all known class type constants
for _, class_type in pairs({
	"CT_Any", "CT_Operator", "CT_Tool", "CT_SourceTool", "CT_SinkTool", 
	"CT_MergeTool", "CT_Modifier", "CT_Mask", "CT_Spline", "CT_Parameter",
	"CT_ImageFormat", "CT_View", "CT_GLViewer", "CT_InputControl",
	"CT_PreviewControl", "CT_Preview", "CT_BinItem", "CT_ExternalControl",
	"CT_Converter", "CT_3D", "CT_3DFilter", "CT_3DFilterSource", "CT_Event",
	"CT_EventControl", "CT_Protocol", "CT_Utility", "CT_PaintTool",
	"CT_BrushShape", "CT_BrushMode", "CT_ApplyMode", "CT_AnimSegment",
	"CT_FlowType", "CT_Locale", "CT_PreviewMedia", "CT_LayoutItem", 
	"CT_Transition", "CT_ToolViewInfo", "CT_ParticleStyle", "CT_ParticleTool",
	"CT_ParticleMergeTool", "CT_ParticleSource", "CT_Region3D", 
	"CT_LightData3D", "CT_Light3D", "CT_MtlData3D", "CT_MtlParticle3D",
	"CT_MtlInputs3D", "CT_CameraData3D", "CT_Camera3D", "CT_CurveData3D",
	"CT_Curve3D", "CT_SurfaceData3D", "CT_SurfaceInputs3D", "CT_Renderer3D",
	"CT_RendererInputs3D", "CT_RenderContext3D", "CT_Shader3D", 
	"CT_FileFormat3D", "CT_ShadowClass3D", "CT_GLTexture", "CT_MtlSW3D",
	"CT_MtlGL3D", "CT_LightSW3D", "CT_LightGL3D", "CT_FuMenu", 
	"CT_ConsoleUtility", "CT_ViewLUTPlugin", "CT_UserControl", "CT_LUTFormat",
	"CT_Prefs"}) do
	
	-- Some class types are no longer available in v17.1
	if (app[class_type]) then
		class_type_index[app[class_type]] = class_type
	end
end

-- The attribute descriptions are from the Fusion 8 Scripting Guide:
-- https://documents.blackmagicdesign.com/UserManuals/Fusion8_Scripting_Guide.pdf
-- We'll use these as tooltips when listing attributes
local registry_attribute_descriptions =
{
	REGS_Name = "Specifies the full name of the class represented by this registry entry.",
	REGS_ScriptName = "Specifies the scripting name of the class represented by this registry entry.\nIf not specified, the full name defined by REGS_Name is used.",
	REGS_HelpFile = "The help file and ID for the class.",
	REGI_HelpID = "The help file and ID for the class.",
	REGS_HelpTopic = "The help file and ID for the class.", -- Was REGI_HelpTopicID in the Scripting Guide
	REGS_OpIconString = "Specifies the toolbar icon text used to represent the class.",
	REGS_OpDescription = "Specifies a description of the class.",
	REGS_OpToolTip = "Specifies a tooltip for the class to provide a longer name or description.",
	REGS_Category = "Specifies the category for the class, defining a position in the Tools menu for tool classes.",
	REGI_ClassType = "Specifies the type of this class, based on the classtype constants.",
	REGI_ClassType2 = "Specifies the type of this class, based on the classtype constants.",
	REGS_ID = "A unique ID for this class.", -- Was REGI_ID in the Scripting Guide
	REGI_OpIcon = "A resource ID for a bitmap to be used for toolbar images for this class.", -- Was REGI_OpIconID in the Scripting Guide
	REGS_IconID = "A resource ID for a bitmap to be used for toolbar images for this class.", -- Was REGI_OpIconID in the Scripting Guide
	REGB_OpNoMask = "Indicates if this Tool class cannot deal with being masked.",
	REGI_DataType = "table Specifies a data type RegID dealt with by this class.",
	REGI_TileID = "Specifies a resource ID used for the tile image by this class.",
	REGB_CreateStaticPreview = "Indicates that a preview object is to be created at startup of this type.",
	REGB_CreateFramePreview = "Indicates that a preview object is to be created for each new frame window.",
	REGB_Preview_CanDisplayImage = "Defines various capabilities of a preview class.",
	REGB_Preview_CanCreateAnim = "Defines various capabilities of a preview class.",
	REGB_Preview_CanPlayAnim = "Defines various capabilities of a preview class.",
	REGB_Preview_CanSaveImage = "Defines various capabilities of a preview class.",
	REGB_Preview_CanSaveAnim = "Defines various capabilities of a preview class.",
	REGB_Preview_CanCopyImage = "Defines various capabilities of a preview class.",
	REGB_Preview_CanCopyAnim = "Defines various capabilities of a preview class.",
	REGB_Preview_CanRecord = "Defines various capabilities of a preview class.",
	REGB_Preview_UsesFilenames = "Defines various capabilities of a preview class.",
	REGB_Preview_CanNetRender = "Defines various capabilities of a preview class.",
	REGI_Version = "Defines the version number of this class or plugin.",
	REGI_PI_DataSize = "Defines a custom data size for AEPlugin classes.",
	REGB_Unpredictable = "Indicates if this tool class is predictable or not. Predictable tools will\ngenerate the same result given the same set of input values, regardless of time.",
	REGI_InputDataType = "Specifies a data type RegID dealt with by the main inputs of this class.",
	REGB_OperatorControl = "Indicates if this tool class provides custom overlay control handling.",
	REGB_Source_GlobalCtrls = "Indicates if this source tool class has global range controls.",
	REGB_Source_SizeCtrls = "Indicates if this source tool class has image resolution controls.",
	REGB_Source_AspectCtrls = "Indicates if this source tool class has image aspect controls.",
	REGB_NoAutoProxy = "Indicates if this tool class does not want things to be autoproxied when it is adjusted.",
	REGI_Logo = "Specifies a resource ID of a company logo for this class.",
	REGI_Priority = "Specifies the priority of this class on the registry list.",
	REGB_NoBlendCtrls = "Indicates if this tool class does not have blend controls.",
	REGB_NoObjMatCtrls = "Indicates if this tool class does not have Object/Material selection controls.",
	REGB_NoMotionBlurCtrls = "Indicates if this tool class does not have Motion Blur controls.",
	REGB_NoAuxChannels = "Indicates if this tool class cannot deal with being given Auxiliary channels (such as Z, ObjID, etc)",
	REGB_EightBitOnly = "Indicates if this tool class cannot deal with being given greater than 8 bit per channel images.",
	REGB_ControlView = "Indicates if this class is a control view class.",
	REGB_NoSplineAnimation = "Specifies that this data type (parameter class) cannot be animated using a spline.",
	REGI_MergeDataType = "Specifies what type of data this merge tool class is capable of merging.",
	REGB_ForceCommonCtrls = "Forces the tool to have common controls like motion blur, blend etc, even on modifiers.",
	REGB_Particle_ProbabilityCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_SetCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_AgeRangeCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_RegionCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_RegionModeCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_StyleCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_EmitterCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGB_Particle_RandomSeedCtrls = "Specifies that particle tools should have (or not have) various standard sets of controls.",
	REGI_Particle_DefaultRegion = "Specifies the RegID of a default Region for this particle tool class.",
	REGI_Particle_DefaultStyle = "Specifies the RegID of a default Style for this particle tool class.",
	REGI_MediaFormat_Priority = "Specifies the priority of a media format class.",
	REGS_MediaFormat_FormatName = "Specifies the name of a media format class",
	REGST_MediaFormat_Extension = "Specifies the extensions supported by a media format class",
	REGB_MediaFormat_CanLoad = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSave = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanLoadMulti = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSaveMulti = "Specify various capabilities of a media format class",
	REGB_MediaFormat_WantsIOClass = "Specify various capabilities of a media format class",
	REGB_MediaFormat_LoadLinearOnly = "Specify various capabilities of a media format class",
	REGB_MediaFormat_SaveLinearOnly = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSaveCompressed = "Specify various capabilities of a media format class",
	REGB_MediaFormat_OneShotLoad = "Specify various capabilities of a media format class",
	REGB_MediaFormat_OneShotSave = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanLoadImages = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSaveImages = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanLoadAudio = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSaveAudio = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanLoadText = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSaveText = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanLoadMIDI = "Specify various capabilities of a media format class",
	REGB_MediaFormat_CanSaveMIDI = "Specify various capabilities of a media format class",
	REGB_MediaFormat_ClipSpecificInputValues = "Specify various capabilities of a media format class",
	REGB_MediaFormat_WantsUnbufferedIOClass = "Specify various capabilities of a media format class",
	REGB_ImageFormat_CanLoadFields = "Specify various capabilities of an image format class",
	REGB_ImageFormat_CanSaveField = "Specify various capabilities of an image format class",
	REGB_ImageFormat_CanScale = "Specify various capabilities of an image format class",
	REGB_ImageFormat_CanSave8bit = "Specify various capabilities of an image format class",
	REGB_ImageFormat_CanSave24bit = "Specify various capabilities of an image format class",
	REGB_ImageFormat_CanSave32bit = "Specify various capabilities of an image format class",
}

-- Counts the occurrences of the given pattern in a string
local function count(text, pattern)
	return select(2, text:gsub(pattern, ""))
end

-- Split and trim functions are loaded from strings since we need to use them in two different contexts
local split_function = 
[[return function(text, delimiter) -- Splits a string on a delimiter into a table, the delimiter is removed
	local result = {};

	-- Escape all non alphanumeric characters in the delimiter, we need this escaped in the gmatch function below
	local escaped_delimiter = delimiter:gsub("([^%w])", "%%%1")

	for match in (text..delimiter):gmatch("(.-)"..escaped_delimiter) do
		table.insert(result, match)
	end

	return result;
end
]]

local trim_function = 
[[return function(str) -- bmd.trim isn't available in all contexts
	return str:gsub("^%s+", ""):gsub("%s+$", "")
end
]]

local split = loadstring(split_function)()
local trim = loadstring(trim_function)()

-- Gets the help information for all classes
local function get_classes()
	local classes = {}
	
	for i, class_name in ipairs(app:GetHelpRaw()) do
		local help = app:GetHelpRaw(class_name)

		-- Store with numerical id
		classes[i] =
		{
			name = class_name,
			path = "",
			parent = help.Parent,
			has_members = #help.Members > 0
		}

		-- Store a reference with class name as id that we can
		-- use for fast lookup
		local existing_class = classes[class_name]
		if (existing_class) then
			--print("Duplicate class name found: "..class_name)
			-- Duplicates don't matter in practice since GetHelpRaw
			-- doesn't seem to be able to tell them apart anyway
			-- (they have the same inheritance path)
		else
			classes[class_name] = classes[i]
		end
	end
	
	-- Get full inheritance path of each class
	for i, class in ipairs(classes) do
		local stack = table.pack(class)
		local current_path = ""
	
		-- Build the path by following each parent
		while (#stack > 0) do
			local current_class = table.remove(stack)
	
			current_path = "/"..current_class.name..current_path
	
			local parent_class = current_class.parent
	
			if (parent_class) then
				table.insert(stack, classes[parent_class])
			end
		end
	
		class.path = current_path
	end
	
	return classes
end

-- Gets registry attributes for all classes
local function get_registry()
	local registry = {}

	for _, reg in ipairs(app:GetRegList()) do
		if (reg) then
			local reg_attr = reg:GetAttrs()

			if (reg_attr) then
				if (registry[reg_attr.REGS_ID]) then
					--print("Duplicate entry: "..reg_attr.REGS_ID)
				else
					registry[reg_attr.REGS_ID] = reg_attr
				end
			end
		end
	end

	return registry
end

-- Loads TagMap and EnumMap from another context, tries to discover additional methods in the fusionsystem linked library
local function load_maps()
	local version = app:GetVersion()

	if (not version.App) then
		-- For Fusion 9 compatibility
		version.App = "Fusion"
	end

	local fusion_path

	if (version.App == "Resolve" or version[1] >= 16) then
		fusion_path = app:MapPath("FusionLibs:")
	else
		-- Fusion 9
		fusion_path = app:MapPath("Fusion:")
	end

	local temp_folder

	if (ffi.os == "Windows") then
		temp_folder = os.getenv("TEMP").."\\"
	else
		temp_folder = os.getenv("TMPDIR")

		if (ffi.os == "Linux" and not temp_folder) then
			temp_folder = "/tmp/"
		end
	end

	local plugins_folder = "Plugins"

	if (version.App == "Fusion" and ffi.os == "Windows") then
		plugins_folder = plugins_folder..[[\Blackmagic]]
	end

	local data_filename = temp_folder..version.App.."_Class_Browser.luatable"
	local data

	local function update_file(filename)
		-- Declare a sleep function available in the OS (we can't use Fusion():Sleep() because it sleeps the thread we want to wait for)
		ffi.cdef[[ void Sleep(int ms); int poll(struct pollfd *fds,unsigned long nfds,int timeout); ]]
		local sleep = iif(ffi.os == "Windows", function(milliseconds) ffi.C.Sleep(milliseconds) end, function(milliseconds) ffi.C.poll(nil,0,milliseconds) end)

		if (bmd.fileexists(filename)) then
			assert(os.remove(filename))
		end
		
		-- Get the data in a context where EnumMap, TagMap and functions aren't limited as they are when used in scripts after Fusion ~8
		app:Execute(
		[===[
			local fusion_filenames = table.pack
			(
				{
					Windows = "fusionsystem.dll",
					OSX = "libfusionsystem.dylib",
					Linux = "libfusionsystem.so",
				},
				{
					Windows = "fusionoperators.dll",
					OSX = "libfusionoperators.dylib",
					Linux = "libfusionoperators.so",
				},
				{
					Windows = "fusionscript.dll",
					OSX = "fusionscript.so",
					Linux = "fusionscript.so",
				},
				{
					Windows = [[]===]..plugins_folder..[===[\fuses.plugin]],
					OSX = [[]===]..plugins_folder..[===[/fuses.plugin]],
					Linux = [[]===]..plugins_folder..[===[/fuses.plugin]],
				},
				{
					Windows = [[]===]..plugins_folder..[===[\text.plugin]],
					OSX = [[]===]..plugins_folder..[===[/text.plugin]],
					Linux = [[]===]..plugins_folder..[===[/text.plugin]],
				}
			)

			for i = 1, fusion_filenames.n do
				fusion_filenames[i].Windows = [[]===]..fusion_path..[===[]]..fusion_filenames[i].Windows
				fusion_filenames[i].OSX = [[]===]..fusion_path..[===[]]..fusion_filenames[i].OSX
				fusion_filenames[i].Linux = [[]===]..fusion_path..[===[]]..fusion_filenames[i].Linux
			end

			local ffi_prefix = " ffi_"
			local split = loadstring([[]===]..split_function..[===[]])()
			local trim = loadstring([[]===]..trim_function..[===[]])()
			
			local function find_rev(str, findstr)
				local current_index = -1
				local last_index

				repeat
					last_index = current_index
					current_index = str:find(findstr, current_index + 1, true)
				until (current_index == nil)
				
				return last_index
			end

			local function get_fusionsystem_content(fusionsystem)
				local file_handle = assert(io.open(fusionsystem, "rb"))
				local fusionsystem_content = file_handle:read("*all")
				assert(file_handle:close())

				local ffi_str = "ffi_" -- Not the same as ffi_prefix
				
				local first_ffi_index = fusionsystem_content:find(ffi_str, 1, true)
				fusionsystem_content = fusionsystem_content:sub(first_ffi_index)
				
				local last_ffi_index = find_rev(fusionsystem_content, "\0"..ffi_str)
				fusionsystem_content = fusionsystem_content:sub(1, last_ffi_index)

				return { ffi_data = fusionsystem_content } -- Making this a table we can pass by reference between functions
			end

			local function get_member_definition(fusionsystem_content, current_position)
				local start_position_found

				repeat
					local current_character = fusionsystem_content.ffi_data:sub(current_position - 1, current_position - 1)
							
					if (current_character) then
						if (current_character == "\0") then
							start_position_found = true
						else
							current_position = current_position - 1
						end
					else
						break
					end
				until (start_position_found)

				return fusionsystem_content.ffi_data:sub(current_position, fusionsystem_content.ffi_data:find(")", current_position, true))
			end

			local function convert_numeric_types(type_name)
				-- I prefer seeing the original types, but if you want to convert every parameter/return type to the native Lua "number", uncomment these lines
				
				--if (type_name == "uint8" or type_name == "uint16" or type_name == "uint" or type_name == "uint32" or type_name == "uint64") then
				--	return "number"
				--	--return "number (unsigned int)"
				--elseif (type_name == "int8" or type_name == "int16" or type_name == "int" or type_name == "int32" or type_name == "int64") then
				--	return "number"
				--	--return "number (signed int)"
				--elseif (type_name == "float16" or type_name == "float" or type_name == "float32" or type_name == "float64") then
				--	return "number"
				--	--return "number (float)"
				--else
					return type_name
				--end
			end

			local function parse_member_definition(member_definition, member_type, split_on)
				local member_help =
				{
					Type = member_type,
					Definition = member_definition,
					Usage =
					{
						[1] = 
						{
							Returns = {},
							Args = {},
							Description = ""
						}
					},
					LongHelp = "",
					SeeAlso = {},
					ShortHelp = "",
					Description = {},
				}
				
				if (member_type == "Variable") then
					if (split_on:find("_Get_", 1, true)) then
						member_help.VarRead = true
						member_help.VarWrite = false
					elseif (split_on:find("_Set_", 1, true)) then
						member_help.VarRead = false
						member_help.VarWrite = true
					end
				end

				local member_definition_parts = split(member_definition, split_on)
				local return_type_name = member_definition_parts[1]:gsub("void", ""):gsub("const char", "string"):gsub(" %*", ""):gsub("%*", ""):gsub("const ", ""):gsub("bool", "boolean")
				member_help.Usage[1].Returns[1] = { Type = convert_numeric_types(return_type_name) }
				
				local parameters_index = member_definition_parts[2]:find("(", 1, true)
				local member_name = member_definition_parts[2]:sub(1, parameters_index - 1)
				local parameters = split(member_definition_parts[2]:sub(parameters_index + 1, -2), ",")

				for i, parameter in ipairs(parameters) do
					parameter = trim(parameter)
							
					if (#parameter > 0) then
						local parameter_parts = split(parameter, " ")

						if (parameter_parts[#parameter_parts]:gsub("*self", "self") ~= "self") then
							local type_name = (table.concat(parameter_parts, " ", 1, #parameter_parts-1)):gsub("const char", "string"):gsub(" %*", ""):gsub("%*", ""):gsub("const ", ""):gsub("bool", "boolean")

							member_help.Usage[1].Args[#member_help.Usage[1].Args+1] = 
							{
								Type = convert_numeric_types(type_name),
								Name = parameter_parts[#parameter_parts]
							}
						else
							if (not member_help.Class) then
								member_help.Class = parameter_parts[1]
							end
						end
					end
				end

				if (member_help.Class) then
					member_help.Name = member_name:sub(#member_help.Class + 1)
				end

				return member_name, member_help
			end

			local function find_members(fusionsystem_content, property_class_name, member_type, search_for)
				local found_at_position = 0
				local next_search_position = 0

				if (member_type == "Variable") then
					search_for = " "..property_class_name.."_"..search_for.."_"
				end

				local discovered_members = {}

				repeat
					found_at_position, next_search_position = fusionsystem_content.ffi_data:find(search_for, next_search_position + 1, true)
					
					if (found_at_position) then
						local member_definition = get_member_definition(fusionsystem_content, found_at_position)
						
						if (not member_definition:find("[;.\n]")) then
							local member_name, member_info = parse_member_definition(member_definition, member_type, search_for)
							discovered_members[member_name] = member_info
						end
					end
				until (not found_at_position)

				return discovered_members
			end

			local function sort_and_sequence_class_members(class_members)
				local sequenced_class_members = {}
				
				for class_name, class_info in pairs(class_members) do
					local seq = {}
				
					for _, member_info in pairs(class_info) do
						seq[#seq+1] = member_info
					end
				
					table.sort(seq, function(a, b) return a.Name < b.Name end)
				
					sequenced_class_members[class_name] = seq
				end

				return sequenced_class_members
			end

			local function get_members()
				local class_properties = {}
				local class_methods = {}
				
				for _, fusionsystem in ipairs(fusion_filenames) do
					local fusionsystem_content = get_fusionsystem_content(fusionsystem[ffi.os])
					local discovered_methods = find_members(fusionsystem_content, nil, "Function", ffi_prefix)
					
					local class_names = {}

					-- First pass for methods
					for name, method_info in pairs(discovered_methods) do
						local class_name = nil
						local method_name = nil
	
						if (method_info.Class) then
							-- Methods that have an identified class
							class_name = method_info.Class
							method_name = method_info.Name
							method_info.Class = nil
						else
							-- Methods without an identified class are usually constructors or other methods that start with an underscore
							local underscore_position = name:find("_", 1, true)
							
							if (underscore_position) then
								class_name = name:sub(1, underscore_position - 1)
								method_name = name:sub(underscore_position)
								method_info.Name = method_name
							end
						end
	
						if (class_name) then
							-- Store the class name even if we don't find any methods, this way we can search for properties later
							if (not class_names[class_name]) then
								class_names[class_name] = {}
							end
								
							if (method_name ~= "_cast" and method_name ~= "_uncast" and method_name ~= "__gc") then
								if (not class_methods[class_name]) then
									class_methods[class_name] = {}
								end
						
								if (method_name == "__new") then
									method_info.ShortHelp = class_name.." constructor"
									method_info.IsConstructor = true
								end
	
								class_methods[class_name][method_name] = method_info
							end
	
							discovered_methods[name] = nil
						end
					end
					
					-- Second pass for methods - match remaining discovered_methods with global functions
					for name, member in pairs(_G) do
						if (type(member) == "function") then
							local class_name = string.match(name, "_(.-)_")
					
							if (class_name) then
								if (not class_names[class_name]) then
									class_names[class_name] = {}
								end

								local method_name = name:gsub("_"..class_name.."_", "")
					
								if (method_name:sub(1, 1) ~= "_") then
									local method_info = discovered_methods[class_name..method_name]
					
									if (method_info) then
										if (not class_methods[class_name]) then
											class_methods[class_name] = {}
										end
					
										method_info.Name = method_name
										class_methods[class_name][method_name] = method_info
										discovered_methods[class_name..method_name] = nil
									end
								end
							end
						end
					end

					-- Properties
					for class_name, _ in pairs(class_names) do
						local getters = find_members(fusionsystem_content, class_name, "Variable", "Get")
						local setters = find_members(fusionsystem_content, class_name, "Variable", "Set")
				
						local properties = {}
				
						-- Add all getters, include matching setters if available
						for property_name, property_info in pairs(getters) do
							if (setters[property_name]) then
								property_info.VarWrite = true
								setters[property_name] = nil
							end
				
							property_info.Name = property_name
							properties[property_name] = property_info
						end
				
						-- Add remaining setters
						for property_name, property_info in pairs(setters) do
							property_info.Name = property_name
							properties[property_name] = property_info
						end
				
						for property_name, property_info in pairs(properties) do
							if (not class_properties[class_name]) then
								class_properties[class_name] = {}
							end
							
							property_info.Class = nil
							class_properties[class_name][property_name] = property_info
						end
					end
				end

				return { Methods = sort_and_sequence_class_members(class_methods), Properties = sort_and_sequence_class_members(class_properties) }
			end

			local members = get_members()
		]===]..
		"bmd.writefile(\""..filename:gsub("\\", "\\\\").."\", { Script = \""..script_name.."\", AppVersion = app:GetVersion(), EnumMap = EnumMap, TagMap = TagMap, Methods = members.Methods, Properties = members.Properties })")

		local wait_start = os.time()
		local timeout = 10 -- seconds

		-- Wait until the file has been written
		while (not bmd.fileexists(filename)) do
			if (os.time() - wait_start >= timeout) then
				error(string.format("Timeout waiting for data file \"%s\"", filename))
			end

			sleep(200) -- milliseconds
		end

		return bmd.readfile(filename)
	end

	if (bmd.fileexists(data_filename)) then
		data = bmd.readfile(data_filename)

		-- Is the data file created with the same version we're running now?
		if (not (
				data.AppVersion and
				data.AppVersion[1] == version[1] and
				data.AppVersion[2] == version[2] and
				data.AppVersion[3] == version[3] and
				data.AppVersion[4] == version[4] and
				data.Script == script_name and
				data.Properties and
				data.Methods
			)) then
			data = update_file(data_filename)
		end
	else
		-- File wasn't found, create a new one
		data = update_file(data_filename)
	end

	return data
end

-- Updates the documented classes with info from the TagMap and discovered methods/properties
local function update_classes(classes, discovered_class_methods, discovered_class_properties, tag_map)
	local function get_tag_map_parent_path(class_name, path)
		if (not path) then
			path = ""
		end

		for name, value in pairs(tag_map[class_name]) do
			if (value.__parent) then
				local parent_class_name = value.__parent:sub(1, value.__parent:find(".", 1, true) - 1)
				return get_tag_map_parent_path(parent_class_name, "/"..parent_class_name..path)
			end
		end

		return path
	end

	local function add_missing_classes(tbl, is_tag_map)
		for class_name, _ in pairs(tbl) do
			local class_info = classes[class_name]
		
			if (not class_info) then
				local path = "/"..class_name
				local parent = nil

				if (is_tag_map) then
					path = get_tag_map_parent_path(class_name).."/"..class_name
					local path_parts = split(path, "/")
					local root_class = classes[path_parts[2]]
					
					-- Reassign to the true root class if it exists
					if (root_class and count(root_class.path, "/") >= 2) then
						path = root_class.path.."/"..table.concat( { select(3, table.unpack(path_parts)) } , "/")
					end

					parent = path_parts[#path_parts-1]

					if (#parent == 0) then
						parent = nil
					end
				end
				
				classes[#classes+1] =
				{
					path = path,
					has_members = true,
					name = class_name,
					parent = parent,
				}

				classes[class_name] = classes[#classes]
			else
				if (class_info.has_members == false) then
					class_info.has_members = true
				end
			end
		end
	end

	add_missing_classes(tag_map, true)
	add_missing_classes(discovered_class_methods)
	add_missing_classes(discovered_class_properties)
end

-- Adds classes to the flat class tree
local function populate_flat_tree(tree, classes)
	local items = {}
	tree:Clear()
	tree.ColumnCount = 1

	for _, class in ipairs(classes) do
		local item = tree:NewItem()
		item.Text[0] = class.name
		items[#items+1] = item
	end

	tree:AddTopLevelItems(items)
	tree:SortByColumn(0, "AscendingOrder")
end

-- To speed up the creation of the nested tree later, we find all the children of each class
local function get_children(classes)
	for _, parent_class in ipairs(classes) do
		local path_parts = split(parent_class.path, "/")
		local sub_classes = {}

		for _, class in ipairs(classes) do
			if (class.path:sub(1, #parent_class.path + 1) == parent_class.path.."/" and count(class.path, "/") == #path_parts) then
				sub_classes[#sub_classes+1] = class
			end
		end

		parent_class.children = sub_classes
	end
end

-- Adds classes to the nested class tree
local function populate_nested_tree(tree, classes)
	local stack = {}
	
	-- Add root classes to the stack
	for _, class in ipairs(classes) do
		-- Root classes are inconsistent,
		-- the ones that are nil seem out of place
		if (class.parent == nil or class.parent == "") then 
			stack[#stack+1] = class
		end
	end
	
	tree:Clear()
	tree.ColumnCount = 1
	local items = {}

	-- Create a new item from each class on the stack
	while #stack > 0 do
		local class = table.remove(stack)

		local item = tree:NewItem()
		item.Text[0] = class.name
		
		if (class.parent_item) then
			class.parent_item:AddChild(item)
			class.parent_item.Expanded = true
			class.parent_item = nil
		else
			items[#items+1] = item
		end

		-- Add children to the stack
		for _, sub_class in ipairs(class.children) do
			sub_class.parent_item = item
			table.insert(stack, sub_class)
		end
	end
	
	tree:AddTopLevelItems(items)
	tree:SortByColumn(0, "AscendingOrder")
end

-- Gets all items (useful for getting all items in a nested tree without traversing the tree)
local function get_all_items(tree)
	return tree:FindItems("*",
	{
		MatchExactly = false,
		MatchFixedString = false,
		MatchContains = false,
		MatchStartsWith = false,
		MatchEndsWith = false,
		MatchCaseSensitive = false,
		MatchRegExp = false,
		MatchWildcard = true,
		MatchWrap = false,
		MatchRecursive = true,
	}, 0)
end

-- Finds tree items based on the given text, wildcards * and ? are supported
local function find_items(tree, text, column)
	return tree:FindItems(text,
	{
		MatchExactly = false,
		MatchFixedString = false,
		MatchContains = true,
		MatchStartsWith = false,
		MatchEndsWith = false,
		MatchCaseSensitive = false,
		MatchRegExp = false,
		MatchWildcard = text:find("[*?]") ~= nil,
		MatchWrap = false,
		MatchRecursive = true,
	}, iif(column == nil, 0, column))
end

-- Selects an item in the given tree by class name
local function select_item(tree, class_name)
	local found_items = tree:FindItems(class_name, 
	{
		MatchExactly = true,
		MatchFixedString = false,
		MatchContains = false,
		MatchStartsWith = false,
		MatchEndsWith = false,
		MatchCaseSensitive = false,
		MatchRegExp = false,
		MatchWildcard = false,
		MatchWrap = false,
		MatchRecursive = true,
	}, 0)

	if (found_items and #found_items > 0) then
		if (found_items[1].Hidden) then
			return nil, string.format("Item \"%s\" is hidden", class_name)
		else
			found_items[1].Selected = true
			tree:ScrollToItem(found_items[1])
			return true
		end
	else
		return nil, string.format("Item \"%s\" was not found", class_name)
	end
end

-- Deselects any selected items
local function deselect_items(tree)
	local selected_items = tree:SelectedItems()
	
	if (#selected_items > 0) then
		for _, selected_item in ipairs(selected_items)do
			selected_item.Selected = false
		end
	end

	return #selected_items
end

-- Gets the first item that hasn't been hidden
local function get_first_visible_item(tree)
	for i = 0, tree:TopLevelItemCount() - 1 do
		local item = tree:TopLevelItem(i)
	
		if (not item.Hidden) then
			return item
		end
	end

	return nil
end

-- Hides/unhides items
local function set_item_visibility(tree, classes, hide)
	tree.UpdatesEnabled = false
	
	deselect_items(tree)

	local firstVisibleItem = nil
	
	for i = 0, tree:TopLevelItemCount() - 1 do
		local item = tree:TopLevelItem(i)
	
		if (not (classes[item.Text[0]].has_members) and hide) then
			if (not item.Hidden) then
				item.Hidden = true
			end
		else
			if (item.Hidden) then
				item.Hidden = false
			end
			
			if (not firstVisibleItem) then
				firstVisibleItem = item
			end
		end
	end
	
	if (firstVisibleItem) then
		firstVisibleItem.Selected = true
		tree:ScrollToItem(firstVisibleItem)
	end
	
	tree.UpdatesEnabled = true
	is_filtered = false
end

-- Writes the given text to a file, overwriting any existing file
local function write_string(filename, text)
	if (bmd.fileexists(filename)) then
		assert(os.remove(filename))
	end

	local file_handle = io.open(filename, "a")
	io.output(file_handle)
	io.write(text)
	io.close(file_handle)
end

-- Creates HTML and adds it to the TextEdit widget
local function display_class(class_name)
	local function get_method_name_html(name, arguments)
		if (arguments == nil) then
			arguments = ""
		end

		-- This will only color the parentheses since arguments have their own style
		return name.."<span style='color: #D0D0D0;'>("..arguments..")</span>"
	end

	local function get_return_types(usage)
		local return_types = {}

		if (not usage.Returns) then 
			-- For compatibility with Fusion 9
			return_types[#return_types+1] = usage.Return.Type
		elseif (#usage.Returns > 0) then
			for _, rtn in ipairs(usage.Returns) do
				return_types[#return_types+1] = rtn.Type
			end
		end

		return return_types
	end

	local function get_usage_html(class_name, function_help)
		local rows = {}

		for i, usage in ipairs(function_help.Usage) do
			local return_types = get_return_types(usage)
			local arguments = {}

			if (#usage.Args > 0) then
				for _, argument in ipairs(usage.Args) do
					arguments[#arguments+1] =
						iif(argument.Optional, "<span class='arg_optional'>[", "")..
						"<span class='arg_type'>"..argument.Type.."</span> "..
						"<span class='arg_name'>"..argument.Name.."</span>"..
						iif(argument.Optional, "]</span>", "")
				end
			end
			
			rows[i] = 
				"<div class='description method_usage'>"..
					"<span class='method_return_type'>"..table.concat(return_types, ", ").."</span> "..
					class_name..iif(function_help.IsConstructor, "", ":")..
					"<span class='method'>"..get_method_name_html(iif(function_help.IsConstructor, "", function_help.Name), table.concat(arguments, ", ")).."</span>"..
				"</div>\n"
		end

		return table.concat(rows)
	end
	
	local function get_see_also_html(member_help, css_class)
		local see_also_members = {}

		-- SeeAlso not supported in Fusion 9
		if (member_help.SeeAlso) then
			for _, see_also in ipairs(member_help.SeeAlso) do
				if (#see_also.Text > 0) then
					--TODO: see_also.Text can refer to another class, it's not always a property or method in the *same* class,
					-- see Fusion:CreateMail() that refers to the class MailMessage or Fusion:MapPathSegments() that refers
					-- to Composition:MapPathSegments()

					-- Disabled
					--see_also_members[#see_also_members+1] =
					--	"<span class='"..css_class.."'>"..
					--		iif(member_help.Type:lower() == "function", get_method_name_html(see_also.Text), see_also.Text)..
					--	"</span>"

					-- For now, just get the text instead
					see_also_members[#see_also_members+1] = see_also.Text
				end
			end

			return iif(#see_also_members > 0, "<div class='see_also'>See also: "..table.concat(see_also_members, ", ").."</div>", "")
		else
			return ""
		end
	end

	local function get_description_html(member_help)
		local descriptions = {}

		for _, description in ipairs(member_help.Description) do
			if (#description.Text > 0) then
				descriptions[#descriptions+1] = "<pre class='text description'>"..trim(description.Text).."</pre>\n"
			end
		end

		return table.concat(descriptions)
	end

	local function parse_attribute(attribute_name)
		local attribute_parts = split(attribute_name:sub(4), "_")
		local attribute_type = attribute_parts[1]
		local attribute_name_only = attribute_parts[2]

		if (attribute_type == "S") then
			attribute_type = "string"
		elseif (attribute_type == "B") then
			attribute_type = "boolean"
		elseif (attribute_type == "N") then
			attribute_type = "number (float)"
		elseif (attribute_type == "I") then
			attribute_type = "number (integer)"
		elseif (attribute_type == "H") then
			attribute_type = "handle" --TODO: Pointer?
		elseif (attribute_type == "NT") then
			attribute_type = "table { number (float) }"
		elseif (attribute_type == "IT") then
			attribute_type = "table { number (integer) }"
		elseif (attribute_type == "ST") then
			attribute_type = "table { string }"
		elseif (attribute_type == "BT") then
			attribute_type = "table { bool }"
		else
			attribute_type = "[unknown] (\""..attribute_type.."\")"
		end

		return attribute_type, attribute_name_only
	end

	local function get_attribute_value_html(value)
		if (type(value) == "table") then
			local output_html = dumptostring(value)
			output_html = output_html:gsub("\\n", "\n") -- Fix for Fusion 9 bug in dumptostring()

			-- Remove the first line which is just a table header created by dumptostring()
			-- then replace line breaks with <br /> tags
			return output_html:sub(output_html:find("\n") + 1, 1, true):gsub("\n", "<br />")
		else
			return tostring(value)
		end
	end

	local function display_properties(header, info_text, output, variables_help)
		if (variables_help and #variables_help > 0) then
			table.insert(output, "<h2>"..header.."</h2>")
			if (info_text and #info_text > 0) then
				table.insert(output, "<div class='text info'>"..info_text.."</div>")
			end
			table.insert(output, "<table cellspacing='7'>\n")
			table.insert(output, "<tr><td colspan='2'><hr /></td></tr>\n")

			for _, variable_help in ipairs(variables_help) do
				local return_types = {}
				local types_text = ""

				for _, usage in ipairs(variable_help.Usage) do
					for _, return_type in ipairs(get_return_types(usage)) do
						return_types[#return_types+1] = return_type
					end
				end

				if (#return_types > 0) then
					types_text = string.format("Type%s: %s", iif(#return_types == 1, "", "s"), table.concat(return_types, ", "))
				end

				local access = { text = "", class = "" }

				if (variable_help.VarRead == nil and variable_help.VarWrite == nil) then
					-- No info, don't show anything
				elseif (variable_help.VarRead and variable_help.VarWrite) then
					access = { text = "Read/write", class = "read_write" }
				elseif (variable_help.VarRead and variable_help.VarWrite == false) then
					access = { text = "Read-only", class = "read_only" }
				elseif (variable_help.VarRead == false and variable_help.VarWrite) then
					access = { text = "Write-only", class = "write_only" }
				else
					-- Shouldn't happen
				end

				table.insert(output, "<tr>\n")
				table.insert(output,
					"<td width='250' class='text'>"..
						"<div class='property'>"..variable_help.Name.."</div>"..
						"<div class='property_return_type'>"..types_text.."</div>"..
						"<div class='access "..access.class.."'>"..access.text.."</div>"..
					"</td>\n")
				table.insert(output,
					"<td width='100%' class='text help'>"..
						"<p class='help'>"..variable_help.ShortHelp.."</p>"..
						get_description_html(variable_help)..
						get_see_also_html(variable_help, "property")..
					"</td>\n")
				table.insert(output, "</tr>\n")
				table.insert(output, "<tr><td colspan='2'><hr /></td></tr>\n")
			end

			table.insert(output, "</table>\n")
		end
	end

	local function display_methods(header, info_text, output, class_name, functions_help)
		if (functions_help and #functions_help > 0) then
			table.insert(output, "<h2>"..header.."</h2>")
			if (info_text and #info_text > 0) then
				table.insert(output, "<div class='text info'>"..info_text.."</div>")
			end
			table.insert(output, "<table cellspacing='7'>\n")
			table.insert(output, "<tr><td colspan='2'><hr /></td></tr>\n")

			for _, function_help in ipairs(functions_help) do
				table.insert(output, "<tr>\n")
				table.insert(output,
					"<td width='250' class='text'>"..
						"<div class='method'>"..get_method_name_html(iif(function_help.IsConstructor, class_name, function_help.Name)).."</div>"..
					"</td>\n")
				table.insert(output,
					"<td width='100%' class='text help'>"..
						"<p class='help'>"..function_help.ShortHelp.."</p>"..
						get_usage_html(class_name, function_help)..
						get_description_html(function_help)..
						get_see_also_html(function_help, "method")..
					"</td>\n")
				table.insert(output, "</tr>\n")
				table.insert(output, "<tr><td colspan='2'><hr /></td></tr>\n")
			end

			table.insert(output, "</table>\n")
		end
	end

	local function display_tags(output, class_name)
		local function get_tag_type(type_name, enum_name)
			if (type_name == nil) then
				return nil
			elseif (type_name == "Bool") then
				return "boolean"
			elseif (type_name == "Enum") then
				if (enum_name) then
					return enum_name
				else
					return "enum"
				end
			elseif (type_name == "UInt") then
				return "number (unsigned integer)"
			else
				print("Unknown type: "..type_name)
				return nil
			end
		end

		local function get_enum(enum_fullpath)
			if (enum_fullpath) then
				local enum_value_parts = split(enum_fullpath, ".")
				
				return
				{
					FullPath = enum_fullpath,
					Name = enum_value_parts[2],
					Values = enum_map[enum_value_parts[1]][enum_value_parts[2]]
				}
			else
				return nil
			end
		end

		local tags = tag_map[class_name]

		if (tags) then
			table.insert(output, "<h2>Tag Map</h2>")
			table.insert(output, "<table cellspacing='7'>\n")
			table.insert(output, "<tr><td colspan='3'><hr /></td></tr>\n")

			for method, method_tags in pairs(tags) do
				local parent_tag = nil
				local parent_tag_html = ""

				for key, value in pairs(method_tags) do
					if (key == "__parent") then
						parent_tag = value
						break
					end
				end
				
				if (parent_tag) then
					parent_tag_html = "<div class='tag_return_type'> : "..parent_tag.."</div>"
				end

				table.insert(output,
					"<tr>\n"..
						"<td width='250' class='text'>"..
							"<div class='tag_method'>"..method.."</div>"..
							parent_tag_html..
						"</td>\n"..
						"<td width='250'></td>"..
						"<td width='100%'></td>"..
					"</tr>\n")
				table.insert(output, "<tr><td colspan='3'><hr /></td></tr>\n")

				for key, value in pairs(method_tags) do
					if (type(key) == "number") then
						local tag_table = method_tags[value]
						local key_type = get_tag_type(tag_table.Type, tag_table.Enum)
						local key_set = tag_table.Set

						table.insert(output,
							"<tr>\n"..
								"<td></td>"..
								"<td class='text'>\n"..
									"<div class='help tag'>"..value.."</div>\n")
						
						if (key_type) then
							table.insert(output, "<div class='tag_return_type'>Type: "..key_type.."</div></td>\n")
						
							local enum = get_enum(tag_table.Enum)

							if (enum) then
								table.insert(output, "<td><dl><dt class='text help'>Possible "..enum.Name.." values:<br /></dt>\n")

								for enum_index, enum_value in pairs(enum.Values) do
									if (type(enum_index) == "number") then
										table.insert(output, "<dd class='text description'>\t"..enum_value.."</dd>\n")
									end
								end

								table.insert(output, "</dl></td>\n")
							else
								table.insert(output, "<td></td>\n")
							end
						else
							table.insert(output, "</td>\n")
						end

						table.insert(output, "</tr>\n")
						table.insert(output, "<tr><td colspan='3'><hr /></td></tr>\n")
					end
				end
			end

			table.insert(output, "</table>\n")
		end
	end

	local function display_registry(output, class_name)
		local reg_attributes = registry_index[class_name]

		if (reg_attributes) then
			table.insert(output, "<h2>Registry Attributes</h2>")
			table.insert(output, "<table cellspacing='7'>\n")
			table.insert(output, "<tr><td colspan='2'><hr /></td></tr>\n")

			local sorted_attributes = {}

			for key, value in pairs(reg_attributes) do
				if (#tostring(value) > 0 or attribute_type == "string") then -- Ignore attributes like empty integers
					local attribute_type, attribute_name = parse_attribute(key)
					sorted_attributes[#sorted_attributes+1] = { Name = attribute_name, FullName = key, Type = attribute_type, Value = value }
				end
			end

			-- Sort by name without the REG prefix
			table.sort(sorted_attributes, function(a, b) return a.Name < b.Name end)

			for _, attribute in ipairs(sorted_attributes) do
				local class_type_string = ""
				local tooltip = registry_attribute_descriptions[attribute.FullName] or ""

				-- Show the enum value that equals the current ClassType, if it's a known value
				if (attribute.FullName == "REGI_ClassType") then
					local class_type_by_id = class_type_index[attribute.Value]
					
					if (class_type_by_id) then
						class_type_string = " = "..class_type_by_id
					end
				end

				table.insert(output, "<tr>\n")
				table.insert(output,
					"<td width='250' class='text'>"..
						"<div class='attribute' title='"..tooltip.."'>"..attribute.FullName.."</div>"..
						"<div class='attribute_return_type'>Type: "..attribute.Type.."</div>"..
					"</td>\n")
				table.insert(output,
					"<td width='100%' class='text attribute_value'>"..get_attribute_value_html(attribute.Value)..iif(class_type_string, class_type_string, "").."</td>\n")
				table.insert(output, "</tr>\n")
				table.insert(output, "<tr><td colspan='2'><hr /></td></tr>\n")
			end

			table.insert(output, "</table>\n")
		end
	end

	local function get_discovered_members(class_name, discovered_class_members)
		local discovered_members = discovered_class_members[class_name]
		
		-- We need to use a copy of the table so we can move parameters to the main Methods/Properties if needed, without affecting the original table
		if (discovered_members) then
			return { table.unpack(discovered_members) }
		else
			return nil
		end
	end

	local function get_discovered_member_index(member_name, discovered_members)
		for i, member_info in ipairs(discovered_members) do
			if (member_info.Name == member_name) then
				return i
			end
		end

		return nil
	end

	local class = classes[class_name]
	local class_inheritance = split(class.path:sub(2), "/")
	local class_help = app:GetHelpRaw(class.name)
	local output = {}

	table.insert(output, "<body bgcolor='#212126'>\n")
	table.insert(output,
	[[
		<style>
			body { font-family: Segoe UI, SegoeUI, Segoe WP, Helvetica Neue, Helvetica, Tahoma, Arial, sans-serif; }
			h1 { color: #D0D0D0; font-size: 40px; font-weight: 600; }
			h2 { color: #D0D0D0; margin-top: 24px; font-size: 28px; font-weight: 600; }
			h3 { color: #D0D0D0; font-size: 16px; font-weight: 400; }
			td { font-size: 13px; font-weight: 400; vertical-align: text-top; }
			th { font-size: 24px; font-weight: 600; }
			pre { white-space: pre-wrap; }

			.page { margin-left: 10px; }
			.text { font-family: ]]..iif(ffi.os == "Windows", "Consolas, Lucida Console, Courier New", iif(ffi.os == "OSX", "SF Mono, Andale Mono, Courier", "Courier New, Monospace"))..[[; font-size: 14px; }
			.help { font-weight: 600; color: #8DCEAA; }
			.description { font-weight: 600; color: #448F65; }
			.see_also { margin-top: 24px; color: #448F65; }
			.info { margin-left: 2px; font-size: 12px; color: #D0D0D0; }

			.parent { color: #D0D0D0; }
			.parent_disabled { color: #919191; }

			.property { font-weight: 600; color: #D0D0D0; }
			.property_return_type { font-size: 12px; color: #7E7E7E; }
			.access { margin-top: 8px; font-size: 12px; }
			.read_write { color: #4376A1; }
			.read_only { color: #E64B3D; }
			.write_only { color: #EB6E00; }

			.method { font-weight: 600; color: #009899; }
			.method_usage { margin: 12px 0px 12px 0px; }
			.method_return_type { color: #7E7E7E; }
			.arg_type { color: #7E7E7E; }
			.arg_name { color: #D0D0D0; }
			.arg_optional { font-style: italic; }

			.attribute { font-weight: 600; color: #C6A077; }
			.attribute_return_type { font-size: 12px; color: #7E7E7E; }
			.attribute_value { color: #D0D0D0; }

			.tag { font-weight: 600; color: #C6A077; }
			.tag_return_type { font-size: 12px; color: #7E7E7E; }
			.tag_method { color: #D0D0D0; }
		</style>
	]])

	table.insert(output, "<div class='page'>\n")
	table.insert(output, "<h1>"..class.name.."</h1>\n")
	
	if (class.parent and #class.parent > 0) then
		table.insert(output, "<h3>&nbsp;")

		for i = #class_inheritance - 1, 1, -1 do
			if (classes[class_inheritance[i]].has_members or (not (windowItems.CheckBoxHideEmptyClasses.Checked and windowItems.CheckBoxHideEmptyClasses.Visible))) then
				table.insert(output, " : <a href='displayclass://"..class_inheritance[i].."' class='parent'>"..class_inheritance[i].."</a>")
			else
				table.insert(output, " : <span class='parent_disabled' title='To navigate to this class, uncheck\n\"Hide Classes Without Members\"'>"..class_inheritance[i].."</span>")
			end
		end

		table.insert(output, "</h3>\n")
	end

	local variables_help = {}
	local functions_help = {}
	local discovered_properties = get_discovered_members(class_name, discovered_class_properties)
	local discovered_methods = get_discovered_members(class_name, discovered_class_methods)

	if (class_help) then
		table.insert(output, "<p class='text help'>"..class_help.ShortHelp.."</p>\n")
	
		if (#class_help.Members > 0) then
			table.sort(class_help.Members)
		
			for i, member in ipairs(class_help.Members) do
				local member_help = app:GetHelpRaw(class_name, member)

				if(member_help.Type:lower() == "variable") then
					variables_help[#variables_help+1] = member_help
				elseif (member_help.Type:lower() == "function") then
					functions_help[#functions_help+1] = member_help
				else
					error("GetHelpRaw reported unknown type: "..member_help.Type)
				end
			end
		end
	end

	if (discovered_properties) then
		for _, variable_help in ipairs(variables_help) do
			local discovered_property_index = get_discovered_member_index(variable_help.Name, discovered_properties)

			if (discovered_property_index) then
				-- Don't need this discovered_property since it already exists in variables_help
				table.remove(discovered_properties, discovered_property_index)
			end
		end
	end

	display_properties("Properties", "", output, variables_help)
	display_properties("Discovered Properties", "Discovered properties might be available in many contexts, but most typically in Fuse scripts", output, discovered_properties)

	-- If parameters aren't documented, get them from discovered_methods if available
	if (discovered_methods) then
		for _, function_help in ipairs(functions_help) do
			local discovered_method_index = get_discovered_member_index(function_help.Name, discovered_methods)

			if (discovered_method_index) then
				local discovered_methods_functions_help = discovered_methods[discovered_method_index]

				if (#function_help.Usage == 0 and #discovered_methods_functions_help.Usage > 0) then
					-- Move usage details from the discovered method to the regular method
					function_help.Usage = discovered_methods_functions_help.Usage
					table.remove(discovered_methods, discovered_method_index)
				elseif (#function_help.Usage > 0) then
					-- Don't need this discovered_method since we already have usage details in functions_help
					table.remove(discovered_methods, discovered_method_index)
				end
			end
		end
	end

	display_methods("Methods", "", output, class_name, functions_help)
	display_methods("Discovered Methods", "Discovered methods might be available in many contexts, but most typically in Fuse scripts", output, class_name, discovered_methods)

	display_tags(output, class_name)
	display_registry(output, class_name)

	table.insert(output, "</div>\n")
	table.insert(output, "</body>\n")

	windowItems.TextEditHelp.UpdatesEnabled = false
	windowItems.TextEditHelp.HTML = table.concat(output)
	windowItems.TextEditHelp.UpdatesEnabled = true

	-- Added for convenience, exports the current class to an HTML file
	-- TODO: Add GUI
	--write_string([[c:\Temp\class_browser.html]], windowItems.TextEditHelp.HTML)
end

-- Creates the main window
local function create_window()
	local left_pane_size = 380

	local window = dispatcher:AddWindow(
	{
		ID = "ClassBrowser",
		WindowTitle = script_name,
		WindowFlags =
		{
			Window = true,
		},
		
		-- Uncomment to make it a modal window that doesn't let Resolve kidnap the keyboard
		--WindowModality = "WindowModal",

		ui:VGroup
		{
			ui:HGroup
			{
				Weight = 0,
			
				ui:LineEdit
				{
					Weight = 1,
					ID = "LineEditFilter",
					MinimumSize = { left_pane_size, 0 }, MaximumSize = { left_pane_size, 16777215 },
					PlaceholderText = "Filter class name...",
					Events = { ReturnPressed = true },
				},

				ui:CheckBox
				{
					Weight = 0,
					ID = "CheckBoxHideEmptyClasses",
					Text = "Hide Classes Without Members",
					Checked = true,
				},

				ui:HGap(1, 1),

				ui:Label
				{
					Weight = 0,
					ID = "InfoLink",
					Text = "<a href='https://tiny.cc/classbrowser' style='color: #4376A1;'>About</a>",
					ToolTip = "Go to the forum post associated with this script",
					OpenExternalLinks = true,
					Alignment = { AlignTop = true },
				},
			},

			ui:HGroup
			{
				Weight = 1,

				ui:VGroup
				{
					Weight = 0,

					ui:Tree
					{
						ID = "TreeFlat", 
						Weight = 1,
						HeaderHidden = true,
						MinimumSize = { left_pane_size, 100 },
						Events = { ItemSelectionChanged = true },
					},

					ui:Tree
					{
						ID = "TreeNested", 
						Weight = 1,
						HeaderHidden = true,
						MinimumSize = { left_pane_size, 100 },
						Events = { ItemSelectionChanged = true },
						Hidden = true,
					},
				},
	
				ui:VGroup
				{
					Weight = 1,

					ui:TextEdit
					{
						Weight = 1,
						ID = "TextEditHelp",
						ReadOnly = true,
						Events = { AnchorClicked = true },
						StyleSheet = "QTextEdit { border: 1px solid black; }",
					},
				},
			},

			ui:VGap(1),

			ui:HGroup
			{
				Weight = 0,

				ui:Button
				{
					Weight = 0,
					ID = "ButtonFlat",
					Text = "Flat View",
					Checkable = true,
					AutoExclusive = true,
					MinimumSize = { 100, 1 },
					Checked = true,
					Events = { Toggled = true },
				},

				ui:Button
				{
					Weight = 0,
					ID = "ButtonNested",
					Text = "Nested View",
					Checkable = true,
					AutoExclusive = true,
					MinimumSize = { 100, 1 },
				},

				ui:Label
				{
					MinimumSize = { 1, 22 }, MaximumSize = { left_pane_size - 215, 22 },
					ID = "LabelStatus",
					Text = "",
					Alignment = { AlignVCenter = true, AlignRight = true },
				},
			},
		},
	})

	windowItems = window:GetItems()
	windowItems.ClassBrowser:Resize( { 1250, 600 } )

	-- Shows a status message at the bottom of the window
	local function show_status(message)
		windowItems.LabelStatus.Text = message
	end

	-- Updates the window in case we have switched between flat and nested trees
	local function update_tree_visibility()
		windowItems.CheckBoxHideEmptyClasses.Visible = windowItems.ButtonFlat.Checked
		windowItems.TreeFlat.Hidden = not windowItems.ButtonFlat.Checked
		windowItems.TreeNested.Hidden = windowItems.ButtonFlat.Checked

		windowItems.ClassBrowser:RecalcLayout()
	end
	
	------------------------------------------
	-- Events
	------------------------------------------

	-- Apply/clear filter of the class tree 
	function window.On.LineEditFilter.ReturnPressed(ev)
		local text = windowItems.LineEditFilter.Text
		local tree = iif(windowItems.ButtonFlat.Checked, windowItems.TreeFlat, windowItems.TreeNested)
		local hide = windowItems.CheckBoxHideEmptyClasses.Checked and windowItems.CheckBoxHideEmptyClasses.Visible

		show_status("Filtering, please wait...")
		tree.UpdatesEnabled = false
		
		if (#text == 0) then
			set_item_visibility(tree, classes, hide)
		else
			deselect_items(tree)
				
			-- Hide all items
			for i, item in ipairs(get_all_items(tree)) do
				if (not item.Hidden) then
					item.Hidden = true
				end
			end
		
			-- Unhide found items and their parents
			for i, foundItem in ipairs(find_items(tree, text)) do
				local stack = table.pack(foundItem)
		
				while (#stack > 0) do
					local item = table.remove(stack)
					local class = item.Text[0]

					if (item.Hidden and (classes[class].has_members or not hide)) then
						item.Hidden = false
						item.Expanded = true
					end
		
					local parentItem = item:Parent()
		
					if (parentItem) then
						table.insert(stack, parentItem)
					end
				end
			end

			is_filtered = true
		end
		
		show_status("")
		tree.UpdatesEnabled = true
	end

	-- Hide/unhide items
	function window.On.CheckBoxHideEmptyClasses.Clicked(ev)
		show_status("Please wait...")
		windowItems.LineEditFilter.Text = ""
		set_item_visibility(windowItems.TreeFlat, classes, ev.On)
		show_status("")
	end

	-- Switch between flat/nested tree
	function window.On.ButtonFlat.Toggled(ev)
		show_status("Please wait...")
		windowItems.ClassBrowser.UpdatesEnabled = false
		
		local tree = iif(ev.On, windowItems.TreeFlat, windowItems.TreeNested)
		local other_tree = iif(not ev.On, windowItems.TreeFlat, windowItems.TreeNested)
		local other_tree_selected_items = other_tree:SelectedItems()
		local current_class_name = nil
		
		if (other_tree_selected_items and #other_tree_selected_items > 0) then
			current_class_name = other_tree_selected_items[1].Text[0]
		end

		if (is_filtered) then
			windowItems.LineEditFilter.Text = ""
			set_item_visibility(other_tree, classes, windowItems.CheckBoxHideEmptyClasses.Checked and windowItems.CheckBoxHideEmptyClasses.Visible)
		end

		update_tree_visibility()
		deselect_items(tree)
		
		if (not current_class_name or not select_item(tree, current_class_name)) then
			select_item(tree, get_first_visible_item(tree).Text[0])
		end
		
		show_status("")
		windowItems.ClassBrowser.UpdatesEnabled = true
	end

	-- Display a selected class based on the selection in the flat tree
	function window.On.TreeFlat.ItemSelectionChanged(ev)
		local selected_items = windowItems.TreeFlat:SelectedItems()

		if (selected_items and #selected_items > 0) then
			display_class(selected_items[1].Text[0])
		end
	end

	-- Display a selected class based on the selection nested tree
	function window.On.TreeNested.ItemSelectionChanged(ev)
		local selected_items = windowItems.TreeNested:SelectedItems()

		if (selected_items and #selected_items > 0) then
			display_class(selected_items[1].Text[0])
		end
	end
	
	-- Navigate based on clicks in the HTML (doesn't work in Fusion 9)
	function window.On.TextEditHelp.AnchorClicked(ev)
		if (ev.URL:sub(1,1) == "#") then
			-- Go to an anchor tag on the same "page"
			ev.sender:ScrollToAnchor(ev.URL:sub(2))
		else
			-- Open another class in the tree
			local url_parts = split(ev.URL, "://")
			if (url_parts and #url_parts > 1 and url_parts[1] == "displayclass") then
				local class_name = url_parts[2]
				local tree = windowItems.TreeFlat
				deselect_items(tree)

				if (is_filtered) then
					show_status("Please wait...")
					windowItems.LineEditFilter.Text = ""
					set_item_visibility(tree, classes, windowItems.CheckBoxHideEmptyClasses.Checked)
					show_status("")
				end

				select_item(tree, class_name)
			end
		end
	end

	function window.On.ClassBrowser.Close(ev)
		dispatcher:ExitLoop()
	end

	return window, windowItems
end

-- Creates the splash window
local function create_splash_window()
	local splash_window = dispatcher:AddWindow(
	{
		ID = "Splash",
		Margin = 0,
		Spacing = 0,
		WindowFlags = { SplashScreen = true },
		Events = { UpdateProgress = true },

		ui:VGroup
		{
			ui:Label
			{	
				Weight = 0,
				Text = script_name.." for DaVinci Resolve/Fusion",
				Alignment = { AlignCenter = true },
				StyleSheet = "color: white; font-weight: bold; font-size: 14px; margin-top: 40px;",
			},
			
			ui:Label
			{
				Text = "Creating Awesome Stuff",
				Alignment = { AlignCenter = true },
			},
		},

		ui:Label
		{
			ID = "SplashProgress",
			StyleSheet = "max-height: 1px; background-color: rgb(102, 0, 39);",
		},

		ui:Label
		{
			ID = "SplashBorder",
			StyleSheet = "border: 1px solid black;",
		},
	})

	local splash_window_items = splash_window:GetItems()

	splash_window_items.Splash.UpdatesEnabled = false

	splash_window_items.Splash:Resize( {400, 200} )
	splash_window_items.SplashBorder:Resize( {400, 200} )
	splash_window_items.SplashBorder:Lower()
	splash_window_items.SplashProgress:Move( { 50, 100 } )

	splash_window_items.Splash.UpdatesEnabled = true

	function splash_window.On.Splash.UpdateProgress(ev)
		local label = splash_window_items.SplashProgress
		label:Resize( {ev.Progress * 3, 2})

		label.StyleSheet = label.StyleSheet:gsub("background%-color: rgb%(102, (%d+), 39%);", "background-color: rgb(102, "..math.floor(ev.Progress * 2.21)..", 39);")
	end

	return splash_window, splash_window_items
end

-- Sends an event to the splash window containing progress information
local function update_progress(progress, iterations)
	app.UIManager:QueueEvent(splash_items.Splash, "UpdateProgress", { Progress = progress } )
	
	-- Might want to give more time to the events at certain times
	if (not iterations) then iterations = 1 end
	for i = 1, iterations do
		dispatcher:StepLoop()
	end
end

-- Controller for initialization tasks,
-- progress updates are sent from here
local function initialize()
	local steps = 100 / 6

	splash_window, splash_items = create_splash_window()
	splash_window:Show()

	local maps = load_maps()

	tag_map = maps.TagMap
	enum_map = maps.EnumMap
	discovered_class_methods = maps.Methods
	discovered_class_properties = maps.Properties
	update_progress(1 * steps)
	
	window, windowItems = create_window()
	classes = get_classes()
	update_classes(classes, discovered_class_methods, discovered_class_properties, tag_map)
	update_progress(2 * steps)

	populate_flat_tree(windowItems.TreeFlat, classes)
	update_progress(3 * steps)

	set_item_visibility(windowItems.TreeFlat, classes, windowItems.CheckBoxHideEmptyClasses.Checked)
	update_progress(4 * steps, 2)

	get_children(classes)
	update_progress(5 * steps, 2)
	
	populate_nested_tree(windowItems.TreeNested, classes)
	update_progress(6 * steps, 2)

	registry_index = get_registry()
	update_progress(6 * steps, 2) -- Setting step 6 twice makes it more likely that we have had time to draw the progress bar at 100%

	dispatcher:ExitLoop()
	splash_window:Hide()
end

initialize()

window:Show()
dispatcher:RunLoop()
window:Hide()

collectgarbage()
